package com.alexandreduhem.lydia;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.alexandreduhem.lydia.domain.db.LydiaDatabase;
import com.facebook.stetho.Stetho;

public class LydiaApp extends Application {

    LydiaDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        database = Room.databaseBuilder(getApplicationContext(),
                LydiaDatabase.class, "lydia-db").build();
    }

    public LydiaDatabase getDatabase() {
        return database;
    }
}
