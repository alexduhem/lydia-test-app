package com.alexandreduhem.lydia.domain.repository;

import android.os.HandlerThread;

import com.alexandreduhem.lydia.domain.client.RandomUserClient;
import com.alexandreduhem.lydia.domain.db.AsyncDabataseHelper;
import com.alexandreduhem.lydia.domain.db.UserDao;
import com.alexandreduhem.lydia.domain.model.RandomUserResponse;
import com.alexandreduhem.lydia.domain.model.User;
import com.alexandreduhem.lydia.util.NetworkChecker;
import com.alexandreduhem.lydia.util.NoNetworkException;

import java.util.List;
import java.util.logging.Handler;

public class RamdomUserRepository implements RandomUserClient.Listener, AsyncDabataseHelper.QueryListener {

    private NetworkChecker networkChecker;
    private RandomUserClient client;
    private Listener listener;
    private AsyncDabataseHelper dbHelper;

    public interface Listener {

        void onUsersResponseRetrieveFromCache(RandomUserResponse response);

        void onUsersResponseRetrieve(RandomUserResponse response);

        void onUsersResponseError(Throwable error);
    }

    public RamdomUserRepository(NetworkChecker networkChecker,
                                RandomUserClient client,
                                AsyncDabataseHelper dbHelper) {
        this.networkChecker = networkChecker;
        this.client = client;
        this.dbHelper = dbHelper;
        dbHelper.setQueryListener(this);
        client.setListener(this);
    }

    public void loadUsers(int page) {
        if (networkChecker.isNetworkAvailable()) {
            client.getRandomUsers(page);
        } else {
            listener.onUsersResponseError(new NoNetworkException());
            dbHelper.queryAll();
        }
    }

    @Override
    public void onUsersReceive(final RandomUserResponse response) {
        dbHelper.insert(response.getUsers());
        if (listener != null) {
            listener.onUsersResponseRetrieve(response);
        }
    }

    @Override
    public void onUsersError(Throwable error) {
        if (listener != null) {
            listener.onUsersResponseError(error);
        }
    }

    @Override
    public void onUsersRetrievedFromCache(List<User> users) {
        if (!users.isEmpty()){
            listener.onUsersResponseRetrieveFromCache(new RandomUserResponse(users));
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
