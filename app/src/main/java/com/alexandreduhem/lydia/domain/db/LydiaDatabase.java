package com.alexandreduhem.lydia.domain.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.alexandreduhem.lydia.domain.model.User;

@Database(entities = {User.class}, version = 1)
public abstract class LydiaDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}