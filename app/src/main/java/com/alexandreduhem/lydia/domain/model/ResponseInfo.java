package com.alexandreduhem.lydia.domain.model;

public class ResponseInfo {

    String seed;
    int result;
    int page;
    String version;

    public ResponseInfo() {
    }

    public String getSeed() {
        return seed;
    }

    public int getResult() {
        return result;
    }

    public int getPage() {
        return page;
    }

    public String getVersion() {
        return version;
    }
}
