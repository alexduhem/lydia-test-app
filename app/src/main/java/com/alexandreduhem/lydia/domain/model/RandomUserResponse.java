package com.alexandreduhem.lydia.domain.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RandomUserResponse {

    @SerializedName("results")
    private List<User> users;
    private ResponseInfo info;

    public RandomUserResponse(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public ResponseInfo getInfo() {
        return info;
    }

    public int getPage(){
        return info == null ? 1 : info.getPage();
    }
}
