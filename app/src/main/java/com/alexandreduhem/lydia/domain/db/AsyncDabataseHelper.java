package com.alexandreduhem.lydia.domain.db;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;

import com.alexandreduhem.lydia.domain.model.User;

import java.util.ArrayList;
import java.util.List;

public class AsyncDabataseHelper {

    public static final String KEY_DATA_USERS = "users";

    private Handler uiHandler;
    private HandlerThread asyncThread;
    private Handler asyncHandler;

    public interface QueryListener {
        void onUsersRetrievedFromCache(List<User> users);
    }

    QueryListener queryListener;

    public AsyncDabataseHelper(UserDao userDao) {
        uiHandler = new Handler(Looper.getMainLooper());
        asyncThread = new HandlerThread("dbThread");
        asyncThread.start();
        asyncHandler = new AsyncHandler(asyncThread.getLooper(), userDao);
    }

    public void insert(List<User> users) {
        Message message = asyncHandler.obtainMessage(AsyncHandler.MESSAGE_INSERT);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(KEY_DATA_USERS, new ArrayList<>(users));
        message.setData(bundle);
        asyncHandler.sendMessage(message);
    }

    public void queryAll() {
        asyncHandler.sendEmptyMessage(AsyncHandler.MESSAGE_QUERY);
    }

    public void setQueryListener(QueryListener queryListener) {
        this.queryListener = queryListener;
    }

    class AsyncHandler extends Handler {

        UserDao userDao;

        static final int MESSAGE_INSERT = 1;
        static final int MESSAGE_QUERY = 2;

        AsyncHandler(Looper looper, UserDao userDao) {
            super(looper);
            this.userDao = userDao;
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_INSERT:
                    userDao.deleteAll();
                    userDao.insertAll(msg.getData().<User>getParcelableArrayList(KEY_DATA_USERS));
                    break;
                case MESSAGE_QUERY:
                    final List<User> users = userDao.getAll();
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (queryListener != null) {
                                queryListener.onUsersRetrievedFromCache(users);
                            }
                        }
                    });
                    break;
            }
        }
    }


}
