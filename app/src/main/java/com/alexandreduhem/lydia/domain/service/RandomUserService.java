package com.alexandreduhem.lydia.domain.service;

import com.alexandreduhem.lydia.domain.model.RandomUserResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RandomUserService {

    @GET("1.0")
    Call<RandomUserResponse> getRamdomUsers(@Query("seed") String seed,
                                           @Query("results") int resultNumber,
                                           @Query("page") int page);
}
