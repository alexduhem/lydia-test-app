package com.alexandreduhem.lydia.domain.client;

import com.alexandreduhem.lydia.domain.model.RandomUserResponse;
import com.alexandreduhem.lydia.domain.model.User;
import com.alexandreduhem.lydia.domain.service.RandomUserService;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RandomUserClient {

    public interface Listener {
        void onUsersReceive(RandomUserResponse response);

        void onUsersError(Throwable error);
    }

    private RandomUserService service;
    private Listener listener;

    private static final String BASE_API_URL = "https://randomuser.me/api/";
    private static final String LYDIA_SEED_PATH = "lydia";
    private static final int NUMBER_RESULT_PER_PAGE = 10;

    public RandomUserClient() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(15L, TimeUnit.SECONDS)
                .readTimeout(15L, TimeUnit.SECONDS)
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();
        service = retrofit.create(RandomUserService.class);
    }

    public void getRandomUsers(int page) {
        service.getRamdomUsers(LYDIA_SEED_PATH, NUMBER_RESULT_PER_PAGE, page)
                .enqueue(new Callback<RandomUserResponse>() {
                    @Override
                    public void onResponse(Call<RandomUserResponse> call, Response<RandomUserResponse> response) {
                        if (response.isSuccessful()) {
                            if (listener != null) {
                                listener.onUsersReceive(response.body());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RandomUserResponse> call, Throwable t) {
                        if (listener != null) {
                            listener.onUsersError(t);
                        }
                    }
                });
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
