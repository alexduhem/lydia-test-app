package com.alexandreduhem.lydia.feature.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexandreduhem.lydia.R;
import com.alexandreduhem.lydia.domain.model.User;
import com.alexandreduhem.lydia.util.CircleTransform;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailUserActivity extends AppCompatActivity {


    User user;
    public static String KEY_INTENT_USER = "user";

    @BindView(R.id.detailUserAddress)
    TextView textViewAddress;
    @BindView(R.id.detailUserEmail)
    TextView textViewEmails;
    @BindView(R.id.imageViewUser)
    ImageView imageView;
    @BindView(R.id.detailUserPhone)
    TextView textViewPhone;
    @BindView(R.id.detailUserCell)
    TextView textViewCell;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = getIntent().getParcelableExtra(KEY_INTENT_USER);
        setContentView(R.layout.detail_user_activity);
        ButterKnife.bind(this);
        updateUi();
        setTitle(String.format(getString(R.string.name_surname_format), user.getName().getFirst(), user.getName().getLast()));
    }

    private void updateUi() {
        Picasso.get()
                .load(user.getPicture().getThumbnail())
                .transform(new CircleTransform())
                .into(imageView);

        textViewEmails.setText(user.getEmail());

        textViewAddress.setText(String.format(getString(R.string.address_detail_format),
                user.getLocation().getStreet(),
                user.getLocation().getPostcode(),
                user.getLocation().getCity(),
                user.getLocation().getState()));

        textViewPhone.setText(user.getPhone());
        textViewPhone.setText(user.getCell());
    }
}
