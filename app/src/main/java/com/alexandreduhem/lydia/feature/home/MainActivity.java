package com.alexandreduhem.lydia.feature.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.alexandreduhem.lydia.LydiaApp;
import com.alexandreduhem.lydia.R;
import com.alexandreduhem.lydia.domain.client.RandomUserClient;
import com.alexandreduhem.lydia.domain.db.AsyncDabataseHelper;
import com.alexandreduhem.lydia.domain.db.UserDao;
import com.alexandreduhem.lydia.domain.model.User;
import com.alexandreduhem.lydia.domain.repository.RamdomUserRepository;
import com.alexandreduhem.lydia.feature.detail.DetailUserActivity;
import com.alexandreduhem.lydia.util.AndroidNetworkChecker;
import com.alexandreduhem.lydia.util.Errors;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainPresenter.View, UserRecyclerAdapter.LastItemListener, UserRecyclerAdapter.ClickListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.mainRootView)
    View rootView;

    UserRecyclerAdapter adapter;

    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new UserRecyclerAdapter();
        adapter.setLastItemListener(this);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        buildPresenter();
        presenter.loadUsers();
    }

    private void buildPresenter() {
        RandomUserClient client = new RandomUserClient();
        UserDao userDao = ((LydiaApp) getApplicationContext()).getDatabase().userDao();
        AsyncDabataseHelper dbHelper = new AsyncDabataseHelper(userDao);
        RamdomUserRepository repository = new RamdomUserRepository(new AndroidNetworkChecker(this), client, dbHelper);
        presenter = new MainPresenter(repository);
        presenter.setView(this);
    }

    @Override
    public void displayList(List<User> users) {
        adapter.setUsers(users);
    }

    @Override
    public void addUsers(List<User> users) {
        adapter.addUsers(users);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(int error) {
        String message;
        switch (error) {
            case Errors.ERROR_NO_NETWORK:
                message = getString(R.string.error_not_network_message);
                break;
            case Errors.ERROR_UNKNOWN:
            default:
                message = getString(R.string.error_unknown_message);
                break;
        }
        Snackbar.make(rootView, message, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.Retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.loadUsers();
                    }
                }).show();
    }

    @Override
    public void onLastItemBinded() {
        presenter.loadMoreUsers();
    }

    @Override
    public void onUserClick(User user) {
        startActivity(new Intent(this, DetailUserActivity.class)
                .putExtra(DetailUserActivity.KEY_INTENT_USER, user));
    }
}
