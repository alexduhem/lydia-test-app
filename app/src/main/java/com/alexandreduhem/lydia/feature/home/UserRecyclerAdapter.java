package com.alexandreduhem.lydia.feature.home;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexandreduhem.lydia.R;
import com.alexandreduhem.lydia.domain.model.User;
import com.alexandreduhem.lydia.util.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerAdapter.UserViewHolder> {

    private ArrayList<User> users;

    public interface LastItemListener {
        void onLastItemBinded();
    }

    public interface ClickListener {
        void onUserClick(User user);
    }

    private LastItemListener lastItemListener;
    private ClickListener clickListener;

    public UserRecyclerAdapter() {
        this.users = new ArrayList<>();
    }

    public void addUsers(List<User> users) {
        this.users.addAll(users);
        notifyDataSetChanged();
    }

    public void setUsers(List<User> users) {
        this.users = new ArrayList<>(users);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        holder.setUser(users.get(position));
        if (position == users.size() - 1 && lastItemListener != null) {
            lastItemListener.onLastItemBinded();
        }
    }

    public void setLastItemListener(LastItemListener lastItemListener) {
        this.lastItemListener = lastItemListener;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.userItemTitle)
        TextView textViewTitle;
        @BindView(R.id.userItemSubtitle)
        TextView textViewSubtitle;
        @BindView(R.id.imageView)
        ImageView imageView;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void setUser(final User user) {
            textViewTitle.setText(String.format(itemView.getContext().getString(R.string.name_surname_format),
                    user.getName().getFirst(), user.getName().getLast()));
            textViewSubtitle.setText(user.getEmail());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onUserClick(user);
                }
            });
            Picasso.get()
                    .load(user.getPicture().getThumbnail())
                    .transform(new CircleTransform())
                    .into(imageView);
        }
    }
}
