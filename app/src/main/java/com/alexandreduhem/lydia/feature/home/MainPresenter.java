package com.alexandreduhem.lydia.feature.home;

import com.alexandreduhem.lydia.domain.model.RandomUserResponse;
import com.alexandreduhem.lydia.domain.model.User;
import com.alexandreduhem.lydia.domain.repository.RamdomUserRepository;
import com.alexandreduhem.lydia.util.Errors;
import com.alexandreduhem.lydia.util.NoNetworkException;

import java.util.ArrayList;
import java.util.List;

public class MainPresenter implements RamdomUserRepository.Listener {

    interface View {
        void displayList(List<User> users);

        void addUsers(List<User> users);

        void showProgress();

        void hideProgress();

        void showError(int error);
    }

    private View view;
    private ArrayList<User> users = new ArrayList<>();

    private RamdomUserRepository repository;

    private int currentPage = 1;

    public MainPresenter(RamdomUserRepository repository) {
        this.repository = repository;
        repository.setListener(this);
    }

    public void loadUsers() {
        view.showProgress();
        repository.loadUsers(currentPage);
    }

    public void loadMoreUsers() {
        if (currentPage > 0) {
            view.showProgress();
            repository.loadUsers(currentPage + 1);
        }
    }

    @Override
    public void onUsersResponseRetrieveFromCache(RandomUserResponse response) {
        currentPage = 0;
        view.hideProgress();
        view.displayList(response.getUsers());
    }

    @Override
    public void onUsersResponseRetrieve(RandomUserResponse response) {
        currentPage = response.getPage();
        view.hideProgress();
        if (users.isEmpty()) {
            view.displayList(response.getUsers());
        } else {
            view.addUsers(response.getUsers());
        }
        users.addAll(response.getUsers());
    }

    @Override
    public void onUsersResponseError(Throwable error) {
        view.hideProgress();
        if (error instanceof NoNetworkException){
            view.showError(Errors.ERROR_NO_NETWORK);
        } else {
            view.showError(Errors.ERROR_UNKNOWN);
        }
    }

    public void setView(View view) {
        this.view = view;
    }
}
