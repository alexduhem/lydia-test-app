package com.alexandreduhem.lydia.util;

public interface NetworkChecker {

    boolean isNetworkAvailable();
}
