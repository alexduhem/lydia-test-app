# lydia-test-app

Pour faire un truc avec une archi un peu propre, je pense mettre environ 4h de dev'.

Après avoir relu correctement et découvert qu'il fallait un système de cache, où il va falloir faire du off-thread sans RxJava, 
je vais forcément mettre plus de temps, je vais en profiter pour découvrir Room que je n'ai pas encore utilisé. 
Donc environ 2h en plus.

Lib utilisés :

- Le duo Retrofit/OkHttp/Gson pour les appels réseaux, incontournables.
- Picasso pour afficher les avatar
- Stetho pour debugger les appels réseaux et la BDD.
- ButterKnife pour lier simplement les vues XML au java sans le "findViewById" rébarbatif
- Room pour la gestion de la BDD locale
- Les support library pour l'UI

J'ai dû mettre environ 6-7h pour faire le test, avec une architecture MVP tout en ayant une injection de dépendance 
(sans framework, d'où le code un peu lourd de la méthode buildPresenter() de la MainActivity), 
ça paraît un peu overkill pour une appli comme celle ci mais bon, c'est une habitude que j'ai pris de faire du MVVM ou du MVP

